import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import { expect } from "chai";
import sinon from 'sinon';

import BoardCell from "../../src/components/BoardCell";

let rootContainer;

beforeEach(() => {
  rootContainer = document.createElement("div");
  document.body.appendChild(rootContainer);
});

afterEach(() => {
  document.body.removeChild(rootContainer);
  rootContainer = undefined;
});

describe("App Component Testing", () => {
    it("Renders dead cell", async () => {
        const deadCell = 0;
        const modifyCell = () => {};
        
        act(() => {
            ReactDOM.render(
                <table className="board">
                    <tbody>
                        <tr>
                            <BoardCell
                            value={deadCell}
                            rowIndex={0}
                            columnIndex={0}
                            modifyCell={modifyCell}
                            />
                        </tr>
                    </tbody>
                </table>, rootContainer);
        });
        const td = rootContainer.querySelector(".board-cell");
        const tdDeadClassExists = td.classList.contains("cell-dead");
        expect(tdDeadClassExists).to.equal(true);
    });
    it("Renders alive cell", async () => {
        const aliveCell = 1;
        const modifyCell = () => {};
        
        act(() => {
            ReactDOM.render(
                <table className="board">
                    <tbody>
                        <tr>
                            <BoardCell
                            value={aliveCell}
                            rowIndex={0}
                            columnIndex={0}
                            modifyCell={modifyCell}
                            />
                        </tr>
                    </tbody>
                </table>, rootContainer);
        });
        const td = rootContainer.querySelector(".board-cell");
        const tdDeadClassExists = td.classList.contains("cell-alive");
        expect(tdDeadClassExists).to.equal(true);
    });
    it("click works", async () => {
        const aliveCell = 1;
        const callback = sinon.spy();
        
        act(() => {
            ReactDOM.render(
                <table className="board">
                    <tbody>
                        <tr>
                            <BoardCell
                            value={aliveCell}
                            rowIndex={0}
                            columnIndex={0}
                            modifyCell={callback}
                            />
                        </tr>
                    </tbody>
                </table>, rootContainer);
        });

        const triggerBoardCell = rootContainer.querySelector('.board-cell');
        
        triggerBoardCell.click();

        expect(callback.called).to.be.true;
    });
});
