import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import { expect } from "chai";

import Board from "../../src/components/Board";
import services from "../../src/services/GameOfLife";

let rootContainer;

beforeEach(() => {
  rootContainer = document.createElement("div");
  document.body.appendChild(rootContainer);
});

afterEach(() => {
  document.body.removeChild(rootContainer);
  rootContainer = undefined;
});

describe("App Component Testing", () => {
    it("Renders correct number of rows", async () => {
        const testArray = [[0,0,0],[0,0,0],[0,0,0],[0,0,0]];
        const data = await services.setInitialState(testArray);
        const modifyCell = () => {}
        act(() => {
            ReactDOM.render(<Board data={data} modifyCell={modifyCell} />, rootContainer);
        });
        const trs = rootContainer.querySelectorAll(".board-row");
        expect(trs.length).to.equal(4);
    });
    it("Renders correct number of cells", async () => {
        const testArray = [[0,0,0],[0,0,0],[0,0,0],[0,0,0]];
        const data = await services.setInitialState(testArray);
        const modifyCell = () => {}
        act(() => {
            ReactDOM.render(<Board data={data} modifyCell={modifyCell} />, rootContainer);
        });
        const tds = rootContainer.querySelectorAll(".board-cell");
        expect(tds.length).to.equal(12);
    });
});
