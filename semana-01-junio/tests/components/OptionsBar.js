import React from 'react';
import ReactDOM from 'react-dom';
import { act, Simulate } from 'react-dom/test-utils';
import { expect } from 'chai';
import sinon from 'sinon';

import OptionsBar from "../../src/components/OptionsBar";

let rootContainer;

beforeEach(() => {
  rootContainer = document.createElement("div");
  document.body.appendChild(rootContainer);
});

afterEach(() => {
  document.body.removeChild(rootContainer);
  rootContainer = undefined;
});

describe('Testing options bar', () => {
    it('Check if clean button work correctly', () => {
        const callback = sinon.spy();
        act(() => {
            ReactDOM.render(
            <OptionsBar 
                modifyBoard={callback}
                syncBoard={callback}
                getNextState={callback}
                data={[[]]}
                toggleBlockedBoard={callback}
            />, rootContainer);
        });
        const triggerClean = rootContainer.querySelector('.clean-btn');
        
        triggerClean.click();
        triggerClean.click();

        expect(callback).to.have.property('callCount', 2);
    });
    it('Check if Change Size button work correctly', () => {
        const callback = sinon.spy();
        sinon.stub
        act(() => {
            ReactDOM.render(
            <OptionsBar 
                modifyBoard={callback}
                syncBoard={callback}
                getNextState={callback}
                data={[[]]}
                toggleBlockedBoard={callback}
            />, rootContainer);
        });
        const triggerChangeSize= rootContainer.querySelector('.change-size-btn');
        
        const columnInput= rootContainer.querySelector('.column-input');
        const rowInput= rootContainer.querySelector('.row-input');
        columnInput.value = "5";
        rowInput.value = "5";
        Simulate.change( columnInput )
        Simulate.change( rowInput )

        triggerChangeSize.click();

        expect(callback.called).to.be.true;
    });
    it('Check if next stage button work correctly', () => {
        const callback = sinon.spy();
        const promiseCallback = sinon.stub().resolves('ok');
        act(() => {
            ReactDOM.render(
            <OptionsBar 
                modifyBoard={callback}
                syncBoard={promiseCallback}
                getNextState={callback}
                data={[[]]}
                toggleBlockedBoard={callback}
            />, rootContainer);
        });
        const triggerNext = rootContainer.querySelector('.next-btn');

        triggerNext.click();

        expect(promiseCallback.called).to.be.true;
    });
    it('Check if Interval button work correctly', function(done) {
        this.timeout(4000)
        const callback = sinon.spy();
        const promiseCallback = sinon.stub().resolves('ok');
        act(() => {
            ReactDOM.render(
            <OptionsBar 
                modifyBoard={callback}
                syncBoard={promiseCallback}
                getNextState={callback}
                data={[[]]}
                toggleBlockedBoard={callback}
            />, rootContainer);
        });
        const triggerInterval = rootContainer.querySelector('.interval-btn');
        triggerInterval.click();

        expect(promiseCallback).to.have.property('callCount', 1)
        expect(callback).to.have.property('callCount', 1)

        setTimeout(function () {
            expect(callback).to.have.property('callCount', 2)
            triggerInterval.click();
            done();
        }, 2500);

    });
    
})