import services from "../../src/services/GameOfLife";
import  { expect } from "chai";

describe("Services Testing", () => {
    it("Sets initial state", async function () {
        const testArray = [[0,0,0],[0,0,0]];
        const response = await services.setInitialState(testArray)
        expect(JSON.stringify(response)).to.equal(JSON.stringify(testArray));
    });
    it("Get initial state", async function () {
        const testArray = [[0,0,0],[0,0,0],[0,0,0],[0,0,0]];
        await services.setInitialState(testArray)
        const response = await services.getInitialState()
        expect(JSON.stringify(response)).to.equal(JSON.stringify(testArray));
    });
    it("Get next stage", async function () {
        const testArray = [[0,0,0],[1,1,1],[0,0,0],[0,0,0]];
        const testArrayNextState = [[0,1,0],[0,1,0],[0,1,0],[0,0,0]];
        await services.setInitialState(testArray)
        const response = await services.nextState()
        expect(JSON.stringify(response)).to.equal(JSON.stringify(testArrayNextState));
    });
});
