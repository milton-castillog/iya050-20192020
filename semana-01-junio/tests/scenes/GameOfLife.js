import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import { expect } from "chai";

import GameOfLife from "../../src/scenes/GameOfLife";

let rootContainer;

beforeEach(() => {
  rootContainer = document.createElement("div");
  document.body.appendChild(rootContainer);
});

afterEach(() => {
  document.body.removeChild(rootContainer);
  rootContainer = undefined;
});

describe("App Component Testing", () => {
    it("Renders Game of Life Title", (done) => {
        act(() => {
            ReactDOM.render(<GameOfLife />, rootContainer);
        });
        const h1 = rootContainer.querySelector("h1");
        expect(h1.textContent).to.equal("Game of Life");
        done();
    });
});
