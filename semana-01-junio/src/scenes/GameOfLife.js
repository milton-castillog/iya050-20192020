import React, { useState , useEffect} from 'react';
import Board from '../components/Board';
import OptionsBar from '../components/OptionsBar';
import services from '../services/GameOfLife';
import '../css/scenes/GameOfLife.css';


const GameOfLife = () => {
    const [ board, setBoard ] = useState([[]]);
    const [ boardSync, setBoardSync ] = useState(true); 
    const [ isBoardBlocked, setIsBoardBlocked ] = useState(false); 

    useEffect(()=>{
        services.getInitialState().then((res)=>{
            setBoard(res);
        })    
    }, [])
    
    const modifyBoard = data => {
        return services.setInitialState(data).then((res)=>{
            setBoard(res);
            setBoardSync(true);
        });
    }

    const getNextState = () => {
        return services.nextState().then((res)=>{
            setBoard(res);
        });
    }

    const syncBoard = async () => {
        if(!boardSync) {
            return services.setInitialState(board).then(()=>{
                setBoardSync(true);
            });
        }
    } 

    const modifyCell = (y, x, newValue) => {
        const newBoard = [...board];
        newBoard[y] = newBoard[y].map( (value, index) => index === x ? newValue : value )
        setBoard(newBoard);
        setBoardSync(false);
    }

    const toggleBlockedBoard = () =>{
        setIsBoardBlocked(!isBoardBlocked);
    }
    
    return (
        <div>
            <h1 className="title">
                Game of Life
            </h1>
            <div className="options-container">
                <OptionsBar 
                    modifyBoard={modifyBoard}
                    syncBoard={syncBoard}
                    getNextState={getNextState}
                    data={board}
                    toggleBlockedBoard={toggleBlockedBoard}
                />
            </div>
            <div className="board-container">
                <Board 
                    data={board}
                    modifyCell={isBoardBlocked ? () => alert("Board blocked") : modifyCell}
                />
            </div>            
        </div>
    );
}
export default GameOfLife;
