import React from 'react';
import '../css/components/Board.css';

const isCellALive = value => value === 0;
const getCellClass = state => state ? "cell-dead" : "cell-alive";
const toggle = value => isCellALive(value) ? 1 : 0;

const BoardCell = ({ value, rowIndex, columnIndex, modifyCell }) => (
    <td
    onClick={ () => modifyCell( rowIndex, columnIndex, toggle(value) ) }
    className={ "board-cell " + getCellClass( isCellALive(value) ) } 
    />
);

export default BoardCell;
