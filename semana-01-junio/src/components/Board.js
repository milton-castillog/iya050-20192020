import React from 'react';
import BoardCell from './BoardCell';
import '../css/components/Board.css';

const Board = ({ data, modifyCell }) => {
    const tableContent = data.map((row, rowIndex)=>{
        return (
            <tr
            key={`row-${rowIndex}`}
            className={"board-row"} >
                {
                    row.map( (value, columnIndex) => {
                        return (
                           <BoardCell
                           key={ `cells-${rowIndex}-${columnIndex}` }
                           value={value}
                           rowIndex={rowIndex}
                           columnIndex={columnIndex}
                           modifyCell={modifyCell}
                           /> 
                        )
                    })
                }
            </tr>
        )
    });
    return(
        <table className="board">
            <tbody>
                {tableContent}
            </tbody>
        </table>
    );
}
export default Board;
