import React, { useState , useEffect} from 'react';
import '../css/components/OptionsBar.css';

const cleanArray = arr => arr.map(()=>0);
const cleanTwoDimensionalArray = arr => arr.map(cleanArray);

const isNumber = value => !isNaN(value);
const isBiggerThanOne = value => isNumber(value) ? value > 1 : false;

const OptionsBar = ({ modifyBoard, syncBoard, getNextState, data, toggleBlockedBoard}) => {
    const [ columnSize, setColumnSize ] = useState(data.length);
    const [ rowSize, setRowSize ] = useState(data[0].length);
    const [ isIntervalSet, setIsIntervalSet ] = useState(false);
    const [ lastIntervalRef, setLastIntervalRef ] = useState(0);

    useEffect(() => {
        return () => {
            clearInterval(lastIntervalRef)
        };
    }, []);

    const syncAndGetNextState = () => syncBoard().then( getNextState );

    const modifyBoardSize = () => {
        if( isBiggerThanOne(columnSize) && isBiggerThanOne(rowSize) ){
            const column = Array.from({length: columnSize}, () => 0);
            const newBoard = Array.from({length: rowSize}, () => [...column]);
            modifyBoard(newBoard);
        }
        else{
            alert("Column and row sizes must be numbers and bigger than 1")
        }
        
    }

    const toogleTimer = () => {
        if(isIntervalSet)
            clearInterval(lastIntervalRef)
        else 
            syncBoard().then( ()=>setLastIntervalRef( setInterval(getNextState, 2000) ) )
            
        setIsIntervalSet(!isIntervalSet);
        toggleBlockedBoard();
    }

    return(
        <div className="options">
            <button
            className="option-btn interval-btn"
            onClick={toogleTimer}
            >
            {isIntervalSet ? "End Interval" : "Start Interval"}
            </button> 
            <div>
                <button
                className="option-btn change-size-btn"
                onClick={modifyBoardSize}
                disabled={isIntervalSet}
                >
                Change Size
                </button> 
                Column:
                <input
                onChange={e => setColumnSize(e.target.value)}
                className="input-btn column-input"
                disabled={isIntervalSet}
                />
                Row:
                <input
                onChange={e => setRowSize(e.target.value)}
                className="input-btn row-input"
                disabled={isIntervalSet}
                />
            </div>
            <button
            className="option-btn clean-btn"
            onClick={()=> modifyBoard( cleanTwoDimensionalArray(data) )}
            disabled={isIntervalSet}
            >
                Clean
            </button>
            <button 
            className="option-btn next-btn"
            onClick={syncAndGetNextState}
            disabled={isIntervalSet}
            >
                Next
            </button>
        </div>
    );
}
export default OptionsBar;
