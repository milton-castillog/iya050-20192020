const URL = "http://localhost:8080/life";

const getInitialState = async () => {
    const response = await fetch(URL);
    const content = await response.text();
    return JSON.parse(content);
}

const setInitialState = async (state) => {
    const response = await fetch(URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(state)
    });

    const content = await response.text();
    return JSON.parse(content);
}

const nextState = async () => {
    const response = await fetch(`${URL}/next`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
    });
    const content = await response.text();
    return JSON.parse(content);
}

const services = { getInitialState, setInitialState, nextState }
export default services;