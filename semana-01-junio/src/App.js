import React from 'react';
import GameOfLife from './scenes/GameOfLife'

const App = () => {
  return(
  <div>
    <GameOfLife />
  </div>
  );
}
export default App
