const fetchService = window.fetchService;

const formatUnixTimeToDate = date => {
    const creationDate = new Date(Number(date));
    return creationDate.toDateString();
}

const newsService = {
    getAllNews: async () =>{
        const query = `{ 
            allNews{
                id
                title
                createdAt

            } 
        }` 
        const {success, data} = await fetchService(query)

        if(success===false){
            alert(data)
            return [];
        }

        const mappedData = data.allNews.map(newsData=>{
            return { 
                id: newsData.id,
                imagen: "https://s6.eestatic.com/2020/01/07/omicrono/hardware/Tecnologia-Hardware-Videojuegos-Consolas-PlayStation-Xbox-Hardware_457965160_141916829_1024x576.jpg",
                titulo: newsData.title,
                fecha: formatUnixTimeToDate(newsData.createdAt),
            }
        })
        return mappedData;
    },
    getNewsDetails: async (id) => {
        const query = `{ 
            news(id: ${id}){
                id
                title
                body
                createdAt
                author{
                    name
                }
            } 
        }` 
        
        const {success, data} = await fetchService(query);

        if(success===false){
            alert(data)
            return {};
        }

        const newsData =  data.news;
        const mappedData = { 
            id: newsData.id ,
            imagen: "https://s6.eestatic.com/2020/01/07/omicrono/hardware/Tecnologia-Hardware-Videojuegos-Consolas-PlayStation-Xbox-Hardware_457965160_141916829_1024x576.jpg",
            titulo: newsData.title,
            fecha: formatUnixTimeToDate(newsData.createdAt),
            contenido: newsData.body,
            author: newsData.author
        }
        return mappedData;
    },
    addNews: async ({title, body, authorId}) => {
        authorId = 1; //TEMPORAL CODE SINCE THERE IS NOT USER CONTROL IN THE APPLICATION AS OF NOW
        const query = `mutation { 
            addNews(title: "${title}", body: "${body}", authorId: ${authorId}) {
                id
                title
                body
                createdAt
            } 
        }`

        const {success, data} = await fetchService(query);

        if(success===false){
            alert(data)
            return {};
        }

        return data.addNews;
    }
}

window.newsService = newsService;