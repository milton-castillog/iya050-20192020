const SERVER_URL = "https://nameless-basin-31528.herokuapp.com/";

const fetchService = async query => {
    const response = await fetch(SERVER_URL, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ query }),
    });
    const data = await response.json();
    if(data.errors !== undefined) 
        return {
            success: false,
            data: data.errors[0].message
        }
    else 
        return {
            success: true,
            data: data.data
        }
}

window.fetchService = fetchService;