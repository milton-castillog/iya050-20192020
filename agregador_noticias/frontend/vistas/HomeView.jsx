const { React, ReactDOM,  Navbar, Newcard } = window;
const { getAllNews } = window.newsService;

class HomeView extends React.Component {
  constructor(props) {
    super(props);
    this.alldata = null;
    this.state = {
      newsData: []
    }
  }
  componentDidMount() {
    getAllNews().then(newsData=> this.setState({ newsData }) );
  }
  render() {
    const { newsData } = this.state;
    return (
      <div>
        <Navbar {...this.props}/>
        <div className="cardscontainer">
          {newsData.map(noticia => {
            return (
                <div key={noticia.id} >
                  <Newcard { ...this.alldata = {...this.props , noticia} } />
                </div>
            );    
          })}
        </div>
          
      </div>
    );
  }
}