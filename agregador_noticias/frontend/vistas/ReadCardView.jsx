const { React, ReactDOM,  Navbar, ReadCard } = window;
const { getNewsDetails } = window.newsService;

class ReadCardView extends React.Component {
  constructor(props) {
    super(props);    
    this.state = { 
        noticia: false            
    };
  }
  componentDidMount() {
    getNewsDetails(this.props.match.params.id)
    .then(noticia=> {
      //checking if object is empty and is an object 
      if(Object.keys(noticia).length !== 0 && noticia.constructor === Object)
        this.setState({ noticia }) 
    });
  }
  render() {
    const noticia = this.state.noticia;

    const readCardComponent = noticia === false 
    ? <div>Buscando Noticia</div>
    : <ReadCard {...this.allData = {...this.props , noticia}}/>
    
    return (
      <div>
        <Navbar {...this.props}/>
        {readCardComponent}
      </div>
    );
  }
}