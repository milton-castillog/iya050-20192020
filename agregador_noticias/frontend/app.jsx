const { React, ReactDOM, HomeView, ReadCardView, CreateCardView} = window;
const { BrowserRouter: Router, Link, Switch, Route } = window.ReactRouterDOM;

const App = () => (
  
  <Router>
  
    <Switch>
      <Route exact path="/" component={props => <HomeView {...props}/> }></Route>
      <Route exact path="/Readnewcard/:id" component={props => < ReadCardView {...props}/> }></Route>
      <Route exact path="/create" component={props => <CreateCardView {...props}/> }></Route>
    </Switch>
  </Router>
);

