const { React, ReactDOM } = window;

class Newcard extends React.Component {
    constructor(props) {
        super(props);
        this.noticia = props.noticia;
        this.state = { 
            leida: false            
        };
    }
    componentDidMount() {
        this.handlestorage(this.noticia.id,false,"get");
    }
    handlestorage(noticiaid,noticiastatus,action){
        let states = JSON.parse(localStorage.getItem('states'));
        if(states !== null){
            if (states[noticiaid] != null) {
                if(action=="get"){
                    this.setState({ leida: states[noticiaid]});
                }else{
                    states[noticiaid] = noticiastatus;
                    localStorage.setItem('states',JSON.stringify(states))
                }
            }else{
                states[noticiaid] = false;
                localStorage.setItem('states',JSON.stringify(states))
            }
        }else{
            let statestosave = new Array();
            statestosave[noticiaid] = false;
            localStorage.setItem('states',JSON.stringify(statestosave))
        }
    }
    changeandredirect(noticiasid) {
        this.handlestorage(noticiasid,true,"post");
        this.props.history.push(`/Readnewcard/${noticiasid}`);
    }

    render() {
        const noticia = this.noticia;
        const estado = this.state.leida;
        let msjboton = "Leer más";
        if(estado === true){
            msjboton = "volver a leer";
        }
        return (
        <div>
            <div className="card">
                <img src={noticia.imagen}  style={{ width: "100%"  }} />
                <div className="containercard">
                    <div>
                        <div className="leftdivcontainer"><h3><b>{noticia.titulo}</b></h3><h5 style={{ marginTop: "-2%" }} >{noticia.fecha}</h5></div>
                        
                        <div className="rigthdivcontainer"><button className="buttoncard" onClick={ () => this.changeandredirect(noticia.id) } ><span>{msjboton}</span></button></div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    );
  }
}