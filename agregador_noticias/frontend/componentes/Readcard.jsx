const { React, ReactDOM } = window;

class ReadCard extends React.Component {
    constructor(props) {
        super(props);
        this.noticia = props.noticia;
      }

  render() {
    const noticia = this.noticia;
    return (
    <div>
        <div className="cardrd">
            <div className="containercardrd">
                <div>
                    <div className="leftdivcontainerrd">
                        <h3><b>{noticia.titulo}</b></h3>
                        <h5 style={{ marginTop: "-2%" }}><b>Autor: {noticia.author.name}</b></h5>
                        <h5 style={{ marginTop: "-2%" }} >{noticia.fecha}</h5>
                    </div>
                </div>
            </div>
                <div className="leftdivcontainerrd"><div className="textocontenidord">{noticia.contenido}</div></div>
                <div className="rigthdivcontainerrd"><img src={noticia.imagen}  style={{ width: "96%" ,}} /></div>
                <div className="containercardrd">
                <div>
                    <div className="leftdivcontainerrd"></div>
                    <div className="rigthdivcontainerrd"><button className="buttoncardrd" onClick={() => this.props.history.goBack()}><span>Volver </span></button></div>
                </div>
                
            </div>

            
            
        </div>
    </div>
    );
  }
}