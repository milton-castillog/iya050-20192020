const { React, ReactDOM } = window;

class Navbar extends React.Component {
    constructor(props) {
        super(props);
      }

  render() {
    return (
    <div>
        <div className="nav">
        <input type="checkbox" id="nav-check" />
        <div className="nav-header">
            <div className="nav-title">
                Noticias
            </div>
        </div>
        
        <div className="nav-links">
            <a onClick={() =>this.props.history.push(`/create`)}>Crear Noticia</a>
            <a onClick={() =>this.props.history.push(`/`)}>Inicio</a>
        </div>
        
        </div>
    </div>
    );
  }
}
