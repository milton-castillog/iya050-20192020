const { React, ReactDOM } = window;
const { addNews } = window.newsService;

class Formcrear extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          title: "",
          body: ""
        }
    }
    handleTitleChange(event) {
      this.setState({ title: event.target.value })
    }
    handleBodyChange(event) {
      this.setState({ body: event.target.value })
    }
    handleSubmit(event) {
      event.preventDefault();
      //this function call should also include an authorId input but since
      //there is no authentification implemented it works like this for now
      //be sure to change if it is implemented in the future
      addNews(this.state).then(data=>{
        //checking if object is empty and is an object 
        if(Object.keys(data).length !== 0 && data.constructor === Object){
          alert(`Creada noticia "${data.title}"`)
          this.props.history.push("/")
        }
      })
    }
  render() {
    const { title, body } = this.state;

    return (
    <div>
        <p className="centered-text">
          El servidor es de muy poca capacidad, por favor intentar agregar <b>una o dos</b> letra en cada campo para asegurar su funcionamiento (a lo mucho el servidor podra guardar tres noticias)
        </p>
        <div className="divfrm">
            <form onSubmit={e=> this.handleSubmit(e) }>
                <label htmlFor="titulo">Titulo de la noticia</label>
                <input
                value={title}
                onChange={e=>this.handleTitleChange(e)}
                type="text"
                className="inputfrm" 
                id="titulo" name="titulo"
                placeholder="Ocurre catastrofe en nuestro servidores...." />

                <label htmlFor="lname">Contenido</label>
                <input
                value={body}
                onChange={e=>this.handleBodyChange(e)}
                type="textarea"
                className="textareafrm"
                id="contenido"
                name="contenido"
                placeholder="Todo ocurrio cuando la legion de jator-data decidio atacar..." />
                
                <label htmlFor="subject">Imagen </label>
                <div></div>
                <input type="file" accept="image/png"  />
                <div></div>

                <input type="submit" className="inputfrm" value="Submit" />
            </form>
        </div>

    </div>
    );
  }
}
