# Agregador de noticias
backend-server: https://nameless-basin-31528.herokuapp.com/
frontend-server: https://simple-question-744fa.firebaseapp.com/

## Requisitos de la segunda entrega entrega

Esta entrega consiste en implementar un servidor utilizando GraphQL e integrarlo con una aplicación web.

La funcionalidad de la aplicación resultante debe ser la misma que la descrita en el enunciado de la primera entrega. Sin embargo, en esta entrega todos los datos de la aplicación deben ser manejados por el servidor. Esto implica que:

El proyecto no contendrá stubs con datos estáticos.
El servidor almacenará de forma persistente los datos introducidos en los formularios.
La aplicación mostrará los datos persistidos de esa manera en sus diferentes componentes.
Para evitaros la complejidad de manejar una base de datos, debéis utilizar Volatile como servicio de almacenamiento. Tened en cuenta que, por los límites que tiene Volatile no podréis almacenar imágenes.

La fecha límite para entregar el trabajo es el 15 de abril, inclusive.
