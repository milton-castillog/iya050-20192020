const model = require('./model-factory.js')

const userModel = model("user");
const newsModel = model("news");

module.exports = {
    userModel,
    newsModel
};