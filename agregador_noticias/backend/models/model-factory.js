const persistenceService = require('../persistence-service-adapter/persistence-service.js')

/*
The modelKey must be in a DB_KEY specified in the server, if not, program will throw an error.
This is done because DB is likely to change, so when that time comes you can add a second paramater 
names schema to the model factory to define the shape of each model in the startup code.
However, if for any reason the next DB used has a library in node that takes care of connections and models
and it is used in this project then use that instead and you don't need to use anything in this folder (probably).
*/
const model = (modelKey) => {
    /*
    Here the model startup functionality can be expanded with startup functions like checking DB connections 
    or even restarting the each model with whichever data is wanted
    */
    return {
        getById: async (id) => {
            const data = await persistenceService.getData(modelKey);
            return data[id];
        },
        getAll: async () => {
            const data = await persistenceService.getData(modelKey);
            return Object.values(data);
        },
        find: async (key, value) => {
            const data = await persistenceService.getData(modelKey);
            const dataVal = Object.values(data);
            //casting values to string so that comparision works for all primitive data types
            return dataVal.filter(obj => String(obj[key]) === String(value));
        },
        create: async (data) => {
            return await persistenceService.addData(modelKey, data)
        },
        update: async (id, data) => {
            return await persistenceService.updateData(modelKey, id, data)
        },
        delete: async (id) => {
            return await persistenceService.deleteData(modelKey, id)
        }
    }
};

module.exports = model;