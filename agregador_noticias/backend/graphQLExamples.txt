// get
{
  allNews{
    id
    title
    body
    createdAt
    author{
      id
      name
      news{
        id
        title
      }
    }
  }
}

//delete
mutation {
  deleteNews(id: 1)
}

//update
mutation {
  updateNews(id: 2, title: "m", body: "l"){
    createdAt
    title
    body
    author{
      name
      id
    }
  }
}

//create news
mutation {
  addNews(title: "m", body: "h", authorId: 1){
    createdAt
    title
    author{
      name
      id
    }
  }
}

//create users
mutation {
  addUser(name: "Tetris"){
    name
    id
  }
}