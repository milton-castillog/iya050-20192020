/*
While I know the correct way to do this is to have an .env file
I will just use this config file for the sake of simplicity so that
anyone that comes in the future might easily understand it
*/
const config = {
    DB_KEYS: {
        news: 'a78501c286e1f48b5ce6ca92f3435d2dfb88707bed6d2-news',
        user: 'a78501c286e1f48b5ce6ca92f3435d2dfb88707bed6d2-user',
    },
    APP_ID: "milton.castillo",
    DB_URL: 'https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/pairs',
}
module.exports = config;