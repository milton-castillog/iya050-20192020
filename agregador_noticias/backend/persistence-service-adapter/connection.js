const fetch = require('node-fetch')
const { DB_URL, DB_KEYS, APP_ID} = require('./config.js')

const getURL = key => `${DB_URL}/${DB_KEYS[key]}`;

const fetchData = async (key, fetchOptions) => {
    if(DB_KEYS[key]=== undefined)
        throw Error("Key not configured in config");
    const url = getURL(key);

    try {
        const response = await fetch(url, {
            headers: {
                'Content-Type': 'application/json',
                'x-application-id': APP_ID
            },
            ...fetchOptions
        });
        const data = await response.json();
    
        //In the persistence service case, when retrieving data fails it actually sends an empty details
        //object, so if that object isn't exists then the connection has failed even id there is a response
        if(data.details !== undefined)
            throw Error("no_values")
        
        return JSON.parse(data.value);
    }
    catch {
        throw Error("Database connection failed")
    }
    

};

const fetchGetData = async (key) => await fetchData( key, { method: "GET" } );
const fetchModifyData = async (key, data) => await fetchData( key, { method: "PUT", body: JSON.stringify(data) } );

module.exports = {
    fetchGetData,
    fetchModifyData
};