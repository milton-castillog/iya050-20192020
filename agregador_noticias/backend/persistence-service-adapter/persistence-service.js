const { fetchGetData, fetchModifyData } = require('./connection.js')

const persistenceService = {
    getData: async (key) => {
        const data = await fetchGetData(key);
        delete data.entries
        return data;
    },
    addData: async (key, newData) => {
        const data = await fetchGetData(key);

        const id = Number(data.entries) + 1;
        data[id] = newData;
        data[id].id = id;
        //saving time in UNIX so that it takes little space
        data[id].createdAt = new Date().getTime()

        if(data[id].id === undefined) throw Error("Error creating register");

        data.entries = id;
        
        const updatedData = await fetchModifyData(key, data);
        return updatedData[id];
    },
    updateData: async (key, id, dataToUpdate) => {
        const data = await fetchGetData(key);
        
        if(data[id] === undefined) throw Error("Data doesn't exists");

        data[id] = {...data[id], ...dataToUpdate};

        const updatedData = await fetchModifyData(key, data);
        return updatedData[id];
    },
    deleteData: async (key, id) => {
        const data = await fetchGetData(key);
        delete data[id];
        const updatedData = await fetchModifyData(key, data);
        return updatedData[id] === undefined;
    },
    reset: async (key, data) => {
        if(data!==undefined)
            return await fetchModifyData(key, { entries: 0, ...data});
        else
            return await fetchModifyData(key, { entries: 0 })
    }
}

module.exports = persistenceService;