/*
While I know the correct way to do this is to have an .env file
I will just use this config file for the sake of simplicity so that
anyone that comes in the future might easily understand it
*/
const config = {
    DB_KEYS: {
        news: 'a78501c286e1f48b5ce6ca92f3435d2dfb88707bed6d2ce00ac890e87ce92189',
        user: 'b78501c286e1f48b5ce6ca92f3435d2dfb88707bed6d2ce00ac890e87ce92180'
    },
    DB_URL: 'https://volatile.wtf/',
}
module.exports = config;