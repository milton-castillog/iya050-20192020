const fetch = require('node-fetch')
const { DB_URL, DB_KEYS} = require('./config.js')

const fetchData = async (body) => {
    const response = await fetch(DB_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
    try {
        const data = await response.json();
        return JSON.parse(data.val);
    }
    catch (error) {
        console.log(error)
        //This error is here because volatile didn't give an error describing this behavior but
        //an HTML response that included the whole front page 
        throw Error("Failed parsing JSON (Database likely to have reached character limit of 255)")
    }
};
const fetchGetData = async (key) => await fetchData({key: DB_KEYS[key]});
const fetchModifyData = async (key, data) => await fetchData({key: DB_KEYS[key], val: JSON.stringify(data)});

module.exports = {
    fetchGetData,
    fetchModifyData
};