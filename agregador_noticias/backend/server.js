const { GraphQLServer } = require('graphql-yoga')
const typeDefs = require('./schema/schema.js')
const resolvers = require('./schema/resolvers.js')

const server = new GraphQLServer({ typeDefs, resolvers });

let port = process.env.PORT;
if (port === null || port === "" || port === undefined)
    port = 3000;
    
server.start({ port });