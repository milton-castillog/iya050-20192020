const { newsModel, userModel } = require('../models/models.js');

const resolvers = {
    Query: {
      news: async (_, args) => await newsModel.getById(args.id),
      allNews: async () => await newsModel.getAll(),
      user: async (_, args) => await userModel.getById(args.id),
      allUsers: async () => await userModel.getAll(),
    },
    Mutation: {
        addUser: async (_, args) => await userModel.create(args),
        updateUser: async (_, args) => await userModel.update(args.id, args),
        deleteUser: async (_, args) => await userModel.delete(args.id),
        addNews: async (_, args) => await newsModel.create(args),
        updateNews: async (_, args) => await newsModel.update(args.id, args),
        deleteNews: async (_, args) => await newsModel.delete(args.id)
    },
    User: {
        news: async root => await newsModel.find("authorId", root.id)
    },
    News: {
        author: async root => await userModel.getById(root.authorId)
    }
}
module.exports = resolvers;

/*
I will not make controllers for each model since this is a simple CRUD api with little logic at the end,
if someone later needs to add logic for treating data before or after a database "transaction" I suggest
to make a new folder call "controllers" then add a file for each of the models. Make a function for
each database operation and modify the data before or after the "transaction" there, afterwards just import
the files here and replace the functions down for those in the controller file.
Just a suggestion though, do whatever you want later if another solution is easier for you.
*/