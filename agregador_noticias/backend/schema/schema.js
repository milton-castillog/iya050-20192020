const typeDefs = `
type Query {
  user(id: ID): User
  news(id: ID): News
  allNews: [News!]!
  allUsers: [User!]!
}
type Mutation {
    addUser(name: String!): User
    updateUser(id: ID!, name: String): User
    deleteUser(id: ID!): Boolean!
    addNews(title: String!, body: String!, authorId: ID!): News
    updateNews(id: ID!, title: String, body: String, authorId: ID): News
    deleteNews(id: ID!): Boolean!
}
type User {
  id: ID!
  name: String!
  news: [News!]!
}
type News {
  id: ID!
  title: String!
  body: String!
  createdAt: String!
  author: User!
}
`
module.exports = typeDefs;