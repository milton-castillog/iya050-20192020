const app = require('./server-config.js');
const routes = require('./server-routes.js');

app.get('/', routes.getAllTodos);
app.get('/:id', routes.getTodo);

app.post('/', routes.postTodo);
app.patch('/:id', routes.patchTodo);

app.delete('/', routes.deleteAllTodos);
app.delete('/:id', routes.deleteTodo);

app.listen(5000, () => console.log(`Listening on port 5000`));

module.exports = app;
