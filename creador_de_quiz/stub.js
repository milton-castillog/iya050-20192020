const quizStub = {
  abc: {
    questions: [
      {
        text: "¿Cual es la batalla mas famosa de Hanibal Barca?",
        duration: 20,
        answers: [
          {
            text: "Waterloo",
            correct: false,
          },
          {
            text: "Cannas",
            correct: true,
          },
          {
            text: "Trebia",
            correct: false,
          },
          {
            text: "Pearl Harbor",
            correct: false,
          },
        ],
      },
      {
        text: "¿Cual es el nombre de la primera perra en ir al espacio?",
        duration: 20,
        answers: [
          {
            text: "Mini",
            correct: false,
          },
          {
            text: "Doggie",
            correct: false,
          },
          {
            text: "Coch",
            correct: false,
          },
          {
            text: "Laika",
            correct: true,
          },
        ],
      },
      {
        text: "1+1 es 5",
        duration: 20,
        answers: [
          {
            text: "Verdadero",
            correct: false,
          },
          {
            text: "Falso",
            correct: true,
          },
        ],
      },
    ],
  },
  dfg: {
    questions: [
      {
        text: "¿Cual es la batalla mas famosa de Hanibal Barca?",
        duration: 20,
        answers: [
          {
            text: "Waterloo",
            correct: false,
          },
          {
            text: "Cannas",
            correct: true,
          },
          {
            text: "Pearl Harbor",
            correct: false,
          },
        ],
      },
      {
        text: "¿Cual es el nombre de la primera perra en ir al espacio?",
        duration: 20,
        answers: [
          {
            text: "Mickey",
            correct: false,
          },
          {
            text: "Doggie",
            correct: false,
          },
          {
            text: "Coch",
            correct: false,
          },
          {
            text: "Laika",
            correct: true,
          },
        ],
      },
      {
        text: "1+1 es 5",
        duration: 20,
        answers: [
          {
            text: "False",
            correct: true,
          },
          {
            text: "True",
            correct: false,
          },
        ],
      },
      {
        text: "¿Cual es la batalla mas famosa de Hanibal Barca?",
        duration: 20,
        answers: [
          {
            text: "Waterloo",
            correct: false,
          },
          {
            text: "Cannas",
            correct: true,
          },
          {
            text: "Trebia",
            correct: false,
          },
          {
            text: "Pearl Harbor",
            correct: false,
          },
        ],
      },
      {
        text: "¿Cual es el nombre de la primera perra en ir al espacio?",
        duration: 20,
        answers: [
          {
            text: "Mickey",
            correct: false,
          },
          {
            text: "Doggie",
            correct: false,
          },
          {
            text: "Coch",
            correct: false,
          },
          {
            text: "Laika",
            correct: true,
          },
        ],
      },
      {
        text: "1+1 es 5",
        duration: 20,
        answers: [
          {
            text: "False",
            correct: true,
          },
          {
            text: "True",
            correct: false,
          },
        ],
      },
    ],
  },
  hij: {
    questions: [
      {
        text: "¿Cual es la batalla mas famosa de Hanibal Barca?",
        duration: 20,
        answers: [
          {
            text: "Waterloo",
            correct: false,
          },
          {
            text: "Cannas",
            correct: true,
          },
          {
            text: "Trebia",
            correct: false,
          },
          {
            text: "Pearl Harbor",
            correct: false,
          },
        ],
      },
      {
        text: "¿Cual es el nombre de la primera perra en ir al espacio?",
        duration: 20,
        answers: [
          {
            text: "Mickey",
            correct: false,
          },
          {
            text: "Doggie",
            correct: false,
          },
          {
            text: "Coch",
            correct: false,
          },
          {
            text: "Laika",
            correct: true,
          },
        ],
      },
      {
        text: "1+1 es 5",
        duration: 20,
        answers: [
          {
            text: "False",
            correct: true,
          },
          {
            text: "True",
            correct: false,
          },
        ],
      },
    ],
  },
};