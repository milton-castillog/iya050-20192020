const { React, ReactDOM } = window;
const { BrowserRouter: Router, Link, Switch, Route } = window.ReactRouterDOM;
const { Navbar, Menu, CreateQuiz, PlayQuiz } =  window;

const QuizSPA = () => {
  return (
    <Router>
      <Navbar />
      <div className="app-body-container">
        <Switch>
          <Route exact path="/">
            <Menu />
          </Route>
          <Route path="/create-quiz">
            <CreateQuiz />
          </Route>
          <Route path="/play-quiz">
            <PlayQuiz />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

ReactDOM.render(<QuizSPA />, document.getElementById("root"));
