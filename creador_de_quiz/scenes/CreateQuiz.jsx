const { React } = window;
const { useState } = React;
const { quizService } = window;
const { CreateQuizSidebar, CreateQuizForm } = window;

const getQuestionInitialState = () => ({
  text: "Pregunta 0",
  answers: [
    {
      text: "",
      correct: false,
    },
    {
      text: "",
      correct: false,
    },
    {
      text: "",
      correct: false,
    },
    {
      text: "",
      correct: false,
    },
  ],
});
const getQuizInitialState = () => [getQuestionInitialState()];

const CreateQuiz = () => {
  const [quiz, setQuiz] = useState(getQuizInitialState());
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);

  const addNewQuestion = () => {
    setQuiz((prevQuiz) => {
      const newQuestion = getQuestionInitialState();
      newQuestion.text = "Pregunta " + prevQuiz.length;
      prevQuiz.push(newQuestion);
      return [...prevQuiz];
    });
  };

  const changeCurrentQuestion = (index) => {
    setCurrentQuestionIndex(index);
  };

  const modifyQuestion = (newQuestion) => {
    setQuiz((prevQuiz) => {
      prevQuiz[currentQuestionIndex] = newQuestion;
      return [...prevQuiz];
    });
    alert("Actualizada");
  };
  const eraseQuestion = (indexToErase) => {
    if (quiz.length <= 1) {
      alert("No se puede hacer cuestionarios sin preguntas");
      return;
    } else if (quiz.length - 1 === currentQuestionIndex) {
      setCurrentQuestionIndex((index) => index - 1);
    }
    setQuiz((prevQuiz) => {
      const newQuiz = prevQuiz.filter((value, index) => indexToErase !== index);
      return newQuiz;
    });
  };

  const addQuiz = () => {
    const result = quizService.create(quiz);
    if (result.status === "success")
      alert(
        "Cuestionario agregado, guarda hash de la partida: " + result.gameCode
      );
  };

  return (
    <div className="create-quiz-container">
      <CreateQuizSidebar
        quiz={quiz}
        sidebarButtonCallback={addNewQuestion}
        clickQuestionCallback={changeCurrentQuestion}
        eraseQuestionCallback={eraseQuestion}
      />
      <div className="form-area">
        <div className="add-new-quiz-container">
          <button className="add-new-quiz-button" onClick={addQuiz}>
            Agregar cuestionario
          </button>
        </div>
        <CreateQuizForm
          question={quiz[currentQuestionIndex]}
          formButtonCallback={modifyQuestion}
        />
      </div>
    </div>
  );
};

window.CreateQuiz = CreateQuiz;
