const { React } = window;
const { useEffect, useState, useRef } = React;
const { playQuizService } = window;
const { PlayQuizHash, PlayQuizQuestion, PlayQuizResult, PlayQuizFinalResults } = window;

const possibleSegments = ["no-game", "question", "result", "ended"];
const getInitialGameStatus = () => ({
  currentSegment: possibleSegments[0],
  corrects: 0,
  incorrects: 0,
});
const PlayQuiz = () => {
  const isInitialMount = useRef(true);
  const [currentQuestion, setCurrentQuestion] = useState({});
  const [questionResults, setQuestionResults] = useState(undefined);
  const [quizHash, setQuizHash] = useState("");
  const [gameStatus, setGameStatus] = useState(getInitialGameStatus());

  useEffect(() => {
    if (isInitialMount.current) isInitialMount.current = false;
    else if (
      gameStatus.currentSegment === possibleSegments[0] ||
      gameStatus.currentSegment === possibleSegments[2]
    ) {
      setGameStatus({ ...gameStatus, currentSegment: possibleSegments[1] });
    } else if (gameStatus.currentSegment === possibleSegments[1]) {
      //I will keep a record of the game overral results in front end
      //so that the next person that works in this can decide
      //to not implement it in backend if they don't want
      if (questionResults.correctAnswer === true) {
        setGameStatus({
          ...gameStatus,
          currentSegment: possibleSegments[2],
          corrects: gameStatus.corrects + 1,
        });
      } else {
        setGameStatus({
          ...gameStatus,
          currentSegment: possibleSegments[2],
          incorrects: gameStatus.incorrects + 1,
        });
      }
    }
  }, [currentQuestion, questionResults]);

  const startGame = () => {
    const response = playQuizService.startPlay(quizHash);
    if (response.status === "found") setCurrentQuestion(response.question);
    else alert("Error, revisa el hash ingresado");
  };

  const sendQuestionAnswer = (answerRef) => {
    const response = playQuizService.verifyAnswer(
      quizHash,
      currentQuestion.ref,
      answerRef
    );
    setQuestionResults(response.result);
  };

  const getNextQuestion = () => {
    const response = playQuizService.getNextQuestion(
      quizHash,
      currentQuestion.ref
    );
    setCurrentQuestion(response.question);
  };

  const getResults = () => {
    //I will keep a record of the game overral results in front end
    //so that the next person that works in this can decide
    //to not implement it in backend if they don't want
    //const response = playQuizService.getQuizResults()
    setGameStatus({
      ...gameStatus,
      currentSegment: possibleSegments[3],
    });
  };

  if (gameStatus.currentSegment === "no-game") {
    const handleHashInputChange = (e) => {
      setQuizHash(e.target.value);
    };
    return (
      <PlayQuizHash
        text={quizHash}
        startGameCallback={startGame}
        onChangeCallback={handleHashInputChange}
      />
    );
  } else if (gameStatus.currentSegment === "question") {
    return (
      <PlayQuizQuestion
        questionData={currentQuestion.data}
        responseCallback={sendQuestionAnswer}
      />
    );
  } else if (gameStatus.currentSegment === "result") {
    return (
      <PlayQuizResult
        result={questionResults}
        nextBtnCallback={getNextQuestion}
        resultsBtnCallback={getResults}
      />
    );
  } else if (gameStatus.currentSegment === "ended") {
    return <PlayQuizFinalResults results={gameStatus} />;
  }
};

window.PlayQuiz = PlayQuiz;