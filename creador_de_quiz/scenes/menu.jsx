const { React } = window;

const Menu = () => {
  return (
    <div className="menu-container">
      <Link to="/create-quiz" className="menu-link">
        <div className="menu-button-container">
          <button className="button menu-button blue">Crear un questionario</button>
        </div>
      </Link>
      <br />
      <Link to="/play-quiz" className="menu-link">
        <div className="menu-button-container">
          <button className="button menu-button red">Resolver cuestionario</button>
        </div>
      </Link>
    </div>
  );
};

window.Menu = Menu;
