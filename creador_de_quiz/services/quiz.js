(() => {
  (quizService = {
    get: (quizHash) => {
      return quizStub[quizHash];
    },
    create: (data) => {
      return {
        status: "success",
        gameCode: "abc",
      };
    },
  }),
    (quizQuestionService = {
      get: (quizHash, questionId) => {
        return quizStub[quizHash].questions[questionId];
      },
      create: (data) => {
        return "success";
      },
    }),
    (playQuizService = {
      startPlay: (quizHash) => {
        const quizToPlay = quizStub[quizHash];
        if (quizToPlay !== undefined) {
          const response = {
            gameHash: quizHash,
            question: {
              ref: 0,
              data: quizToPlay.questions[0],
            },
            status: "found",
            last: false,
          };
          return response;
        } else {
          return {
            status: "not_found",
          };
        }
      },
      verifyAnswer: (quizHash, questionRef, answerRef) => {
        const isLastQuestion = (qRef) =>
          qRef === quizStub[quizHash].questions.length - 1;

        if (answerRef === "answer_timeout") {
          const response = {
            status: "success",
            result: {
              correctAnswer: false,
              lastQuestion: isLastQuestion(questionRef),
            },
          };
          return response;
        }
        const answerResult =
          quizStub[quizHash].questions[questionRef].answers[answerRef]
            .correct === true;

        const response = {
          status: "success",
          result: {
            correctAnswer: answerResult,
            lastQuestion: isLastQuestion(questionRef),
          },
        };
        return response;
      },
      getNextQuestion: (quizHash, questionRef) => {
        const response = {
          gameHash: quizHash,
          question: {
            ref: questionRef + 1,
            data: quizStub[quizHash].questions[questionRef + 1],
          },
        };
        return response;
      },
      //I will keep a record of the game overral results in front end
      //so that the next person that works in this can decide
      //to not implement it in backend if they don't want
      getQuizResults: () => {
        const response = {
          correct: 2,
          incorrect: 1,
        };
        return response;
      },
    });
})(window);
