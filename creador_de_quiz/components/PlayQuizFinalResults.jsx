const { React } = window;

const PlayQuizFinalResults = (props) => {
  return (
    <div className={"play-quiz-container result-correct"}>
      <div className="play-quiz-result-container">
        <h1 className="play-quiz-title result-title">
          {props.results.corrects} Correctas
        </h1>
        <h1 className="play-quiz-title result-title">
          {props.results.incorrects} Incorrectas
        </h1>
        <Link to="/" className="menu-link">
          <button className="button play-quiz-result-button">Ir a inicio</button>
        </Link>
      </div>
    </div>
  );
};

window.PlayQuizFinalResults = PlayQuizFinalResults;
