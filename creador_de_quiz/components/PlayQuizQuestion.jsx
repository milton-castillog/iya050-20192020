const { React } = window;
const { useEffect, useState, useRef } = React;

const useInterval = (callback, period) => {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  });

  useEffect(() => {
    const interval = setInterval(() => savedCallback.current(), period);
    return () => clearInterval(interval);
  }, [period]);
};

const PlayQuizQuestion = (props) => {
  const [time, setTime] = useState(0);
  useEffect(() => {
    if (time >= props.questionData.duration)
      props.responseCallback("answer_timeout");
  }, [time]);

  const ONE_SECOND = 1000; // ms
  useInterval(() => {
    setTime((lastTime) => lastTime + 1);
  }, ONE_SECOND);

  const answersContainers = props.questionData.answers
    .map((answer, index) => {
      return (
        <div className="play-quiz-answer" key={index}>
          <button
            className="button play-quiz-answer-button"
            onClick={() => props.responseCallback(index)}
          >
            {answer.text}
          </button>
        </div>
      );
    })
    .reduce((dividers, answerJsx, index) => {
      if (index % 2 === 0) dividers.push([answerJsx]);
      else dividers[dividers.length - 1].push(answerJsx);
      return dividers;
    }, [])
    .map((dividers, index) => {
      return (
        <div className="play-quiz-answer-container" key={index}>
          {dividers}
        </div>
      );
    });

  return (
    <div className="play-quiz-container">
      <div className="play-quiz-question-container">
        <input
          className="play-quiz-input"
          disabled
          defaultValue={props.questionData.text}
        />
      </div>
      <div className="play-quiz-time-left-container">
        <h1 className="play-quiz-title">{props.questionData.duration - time}</h1>
      </div>
      {answersContainers}
    </div>
  );
};

window.PlayQuizQuestion = PlayQuizQuestion;
