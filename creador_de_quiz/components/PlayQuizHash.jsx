const { React } = window;

const PlayQuizHash = (props) => {
  return (
    <div className="play-quiz-container">
      <div className="play-quiz-hash-container">
        <h1 className="play-quiz-title">Introduce el hash del cuestionario</h1>
        <input
          className="play-quiz-input hash-input"
          value={props.text}
          onChange={props.onChangeCallback}
        />
        <button
          className="button play-quiz-hash-button"
          onClick={props.startGameCallback}
        >
          Entrar a juego
        </button>
      </div>
    </div>
  );
};

window.PlayQuizHash = PlayQuizHash;