const { React } = window;

const PlayQuizResult = (props) => {
  const containerExtraClass = props.result.correctAnswer
    ? "result-correct"
    : "result-incorrect";
  const titleText = props.result.correctAnswer
    ? "Respuesta Correcta"
    : "Respuesta Inorrecta";
  const getButton = () => {
    if (props.result.lastQuestion === false) {
      return (
        <button
          className="button play-quiz-result-button"
          onClick={props.nextBtnCallback}
        >
          Siguiente pregunta
        </button>
      );
    } else {
      return (
        <button
          className="button play-quiz-result-button"
          onClick={props.resultsBtnCallback}
        >
          Obtener Resultados
        </button>
      );
    }
  };

  return (
    <div className={"play-quiz-container " + containerExtraClass}>
      <div className="play-quiz-result-container">
        <h1 className="play-quiz-title result-title">{titleText}</h1>
        {getButton()}
      </div>
    </div>
  );
};

window.PlayQuizResult = PlayQuizResult;
