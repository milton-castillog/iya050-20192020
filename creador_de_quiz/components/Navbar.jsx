const { React } = window;

const Navbar = () => {
  return (
    <nav className="navbar">
      <div className="navbar-items-container">
        <Link to="/" className="return-button">
          <span>Home</span>
        </Link>
      </div>
    </nav>
  );
};

window.Navbar = Navbar;
