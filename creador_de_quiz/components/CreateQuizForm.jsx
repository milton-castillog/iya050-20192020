const { React } = window;
const { useEffect, useState } = React;

const CreateQuizForm = (props) => {
  const [questionText, setQuestionText] = useState(props.question.text);
  const [answers, setAnswers] = useState(props.question.answers);

  useEffect(() => {
    setQuestionText(props.question.text);
    setAnswers(props.question.answers);
  }, [props]);

  const handleQuestionTextChange = (e) => {
    setQuestionText(e.target.value);
  };

  const handleAnswersTextChanges = (e, index) => {
    e.persist();
    setAnswers((prevAnswer) => {
      prevAnswer[index].text = e.target.value;
      return [...prevAnswer];
    });
  };

  const toggleAnswersCorrectProp = (index) => {
    setAnswers((prevAnswer) => {
      prevAnswer[index].correct = !prevAnswer[index].correct;
      return [...prevAnswer];
    });
  };

  const answersContainers = answers
    .map((answer, index) => {
      const extraButtonClass = answer.correct
        ? "correct-answer-button"
        : "incorrect-answer-button";
      const buttonText = answer.correct ? "Correcta" : "Incorrecta";
      return (
        <div className="new-question-answer" key={index}>
          <input
            className="new-question-answer-input"
            value={answer.text}
            onChange={(event) => handleAnswersTextChanges(event, index)}
          />
          <button
            className={"new-question-answer-button " + extraButtonClass}
            onClick={() => toggleAnswersCorrectProp(index)}
          >
            {buttonText}
          </button>
        </div>
      );
    })
    .reduce((dividers, answerJsx, index) => {
      if (index % 2 === 0) dividers.push([answerJsx]);
      else dividers[dividers.length - 1].push(answerJsx);
      return dividers;
    }, [])
    .map((dividers, index) => {
      return (
        <div className="new-question-answer-container" key={index}>
          {dividers}
        </div>
      );
    });

  return (
    <div className="new-question-form-container">
      <div className="new-question">
        <input
          className="new-question-input"
          value={questionText}
          onChange={handleQuestionTextChange}
        />
      </div>
      {answersContainers}
      <div className="add-new-question-container">
        <button
          className="add-new-question-button"
          onClick={() => {
            const updateInfo = {
              text: questionText,
              answers: answers,
            };
            props.formButtonCallback(updateInfo);
          }}
        >
          Modificar pregunta
        </button>
      </div>
    </div>
  );
};

window.CreateQuizForm = CreateQuizForm;