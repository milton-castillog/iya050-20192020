const { React } = window;

const CreateQuizSidebar = (props) => {
  const sidebarItemsGenerator = (question, index) => {
    return (
      <div
        className="sidebar-questions-items"
        key={index}
        onClick={() => props.clickQuestionCallback(index)}
      >
        <div className="sidebar-questions-erase-container">
          <div
            className="sidebar-questions-erase"
            onClick={() => props.eraseQuestionCallback(index)}
          ></div>
        </div>
        <div className="sidebar-questions-p-container">
          <p>{question.text}</p>
        </div>
      </div>
    );
  };
  return (
    <div className="create-quiz-sidebar">
      <div className="sidebar-questions">
        {props.quiz.map(sidebarItemsGenerator)}
      </div>
      <div className="sidebar-button-section">
        <div className="sidebar-button-container">
          <button className="sidebar-button" onClick={props.sidebarButtonCallback}>
            Nueva pregunta
          </button>
        </div>
      </div>
    </div>
  );
};

window.CreateQuizSidebar = CreateQuizSidebar;